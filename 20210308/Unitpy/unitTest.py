# -*- coding:utf-8 -*-
from selenium import webdriver
import unittest
from time import sleep
from ddt import ddt, data, unpack, file_data


# def read_line():
#     # read_file = open('passwd.txt','r',encoding='utf8')
#     with open('passwd.txt','r',encoding='utf8') as read_file:
#         list = []
#         for content in read_file.readlines():
#             # print(content)
#             list.append(content.strip('\n').split(':'))
#         # for l in list:
#         #     print(l)
#     return list


@ddt
class unit_test(unittest.TestCase):
    # def setUp(self) -> None:
    #     self.driver = webdriver.Chrome()
    #     self.driver.get(
    #         "https://login.taobao.com/member/login.jhtml?spm=a21bo.2017.754894437.1.5af911d95zwN7n&f=top&redirectURL=https%3A%2F%2Fwww.taobao.com%2F")
    #     self.driver.implicitly_wait(3)
    #
    # def tearDown(self) -> None:
    #     self.driver.quit()
    #
    # @data(*read_line())
    # @unpack
    # def test_1(self, username, password):
    #     self.driver.find_element_by_name('fm-login-id').send_keys(username)
    #     self.driver.find_element_by_name('fm-login-password').send_keys(password)
    #     self.driver.find_element_by_xpath('/html/body/div/div[2]/div[3]/div/div/div/div[2]/div/form/div[4]/button').click()
    #     sleep(1)

    # def test_2(self):
    #     self.driver.find_element_by_name('fm-login-id').send_keys('66666')
    #     self.driver.find_element_by_name('fm-login-password').send_keys('1233')
    #     self.driver.find_element_by_xpath('/html/body/div/div[2]/div[3]/div/div/div/div[2]/div/form/div[4]/button').click()
    #     sleep(5)

    @file_data('passwd.yaml')
    def test_3(self, **user):
        name = user.get('name')
        pwd = user.get('passwd')
        print('name:', name)
        print('pwd:', pwd)


if __name__ == '__main__':
    unittest.main()
